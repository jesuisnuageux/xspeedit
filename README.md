# XSpeedIt : Oui.sncf

Specs can be found on [github](https://github.com/voyages-sncf-technologies/xspeedit/blob/master/README.md)

### Requirement

- node ( my local version is v10.5.0 )
- yarn

### Usage

With a random value :
```
$ ./xspeedit
Articles       : 676461275579247
Robot actuel   : 6/7/64/612/7/55/7/9/24/7 => 10 cartons utilisés
Robot optimisé : 91/72/72/7/7/64/64/6/55 => 9 cartons utilisés
```

With a specific value :
```
$ ./xspeedit -i 163841689525773
Articles       : 163841689525773
Robot actuel   : 163/8/41/6/8/9/52/5/7/73 => 10 cartons utilisés
Robot optimisé : 91/82/81/73/73/64/6/55 => 8 cartons utilisés
```


