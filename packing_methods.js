/**
 *  Collects functions which can perform the packing of values
 */

const _ = require("lodash");

/**
 *  (Utility) Returns the sum of all the numeric values contained in arr
 *
 * @param arr Array<Number>
 * @returns {Number}
 */
const sum = (arr) => {
    if (arr.length === 0) return 0;
    return arr.reduce((a, b) => a + b, 0);
};

/**
 *  Packs values as they come while respecting the max_per_box constraint
 *
 * @param inputs Array<Number>
 * @param max_per_box Number
 * @returns {Promise<Array<Array<Number>>>}
 */
exports.naive_packing = async (inputs, max_per_box) => {
    return inputs.reduce((acc, e) => {
        if (sum(_.last(acc)) + e > max_per_box) { // test if we can fit more items in the box
            acc.push([]); // if not we add +1 box
        }
        _.last(acc).push(e); // putting the item in the last box
        return acc;
    }, [[]]);
};

/**
 *  Attempts to pack while maximizing the use of the max_per_box limit
 *
 * @param inputs Array<Number>
 * @param max_per_box Number
 * @returns {Promise<Array<Array<Number>>>}
 */
exports.optimised_packing = async (inputs, max_per_box) => {
    inputs = inputs.sort(); // reorder inputs lowest value first
    let output = [];
    while (inputs.length) { // while we have values to process
        let box = [];
        box.push(inputs.pop()); // we populate the new box with the highest value remaining
        // let's go through remaining values as long we still have hope to fit more values in the box ( hence sum(box) )
        for (let i = inputs.length; i >= 0 && sum(box) <= max_per_box;) {
            if (inputs[i] + sum(box) <= max_per_box) { // whenever we find a fit for another value
                box.push(inputs[i]); // we add it
                inputs.splice(i, 1); // and remove it from the remaining values to be processed
            } else {
                i--;
            }
        }
        output.push(box) // we add the box to the output
    }
    return output;
};